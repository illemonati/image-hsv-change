mod utils;

use wasm_bindgen::prelude::*;
use std::f64;
use crate::utils::{convert_hsv_to_rgb, convert_rgb_to_hsv, set_panic_hook};

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;


fn apply_v_transform(v: f64, multiplier: f64, range: f64) -> f64 {
    let vnew = ((multiplier + range / 2.0) / (range / 2.0)) * v;
    return if vnew <= 1.0 { vnew } else { 1.0 };
}

fn apply_s_transform(s: f64, multiplier: f64, range: f64) -> f64 {
    let snew = ((multiplier + range / 2.0) / (range / 2.0)) * s;
    return if snew <= 1.0 { snew } else { 1.0 };
}

fn apply_h_transform(h: f64, multiplier: f64, range: f64) -> f64 {
    let hnew = ((multiplier + range / 2.0) / (range / 2.0)) * h;
    return hnew % 360.0;
}


fn adjust_pixel_hsv(
    h: f64,
    s: f64,
    v: f64,
    hue_multiplier: f64,
    hue_range: f64,
    saturation_multiplier: f64,
    saturation_range: f64,
    value_multipler: f64,
    value_range: f64,
) -> (f64, f64, f64) {
    let vnew = apply_v_transform(v, value_multipler, value_range);
    let snew = apply_s_transform(s, saturation_multiplier, saturation_range);
    let hnew = apply_h_transform(h, hue_multiplier, hue_range);
    (hnew, snew, vnew)
}


#[wasm_bindgen]
pub fn adjust_image_hsv_wasm(
    in_hsv_array: &[u16],
    out_hsv_array: &mut [u16],
    hue_multiplier: f64,
    hue_range: f64,
    saturation_multiplier: f64,
    saturation_range: f64,
    value_multipler: f64,
    value_range: f64,
    array_len: usize,
) {
    for i in (0..array_len).step_by(4) {
        let h = in_hsv_array[i] as f64;
        let s = in_hsv_array[i + 1] as f64;
        let v = in_hsv_array[i + 2] as f64;
        let (h_new, s_new, v_new) = adjust_pixel_hsv(
            h,
            s / 100.0,
            v / 100.0,
            hue_multiplier,
            hue_range,
            saturation_multiplier,
            saturation_range,
            value_multipler,
            value_range,
        );
        out_hsv_array[i] = f64::round(h_new) as u16;
        out_hsv_array[i + 1] = f64::round(s_new * 100.0) as u16;
        out_hsv_array[i + 2] = f64::round(v_new * 100.0) as u16;
        out_hsv_array[i + 3] = in_hsv_array[i + 3];
    }
}

#[wasm_bindgen]
pub fn adjust_image_hsv_from_rgb_wasm(
    in_rgb_array: &[u8],
    out_rgb_array: &mut [u8],
    hue_multiplier: f64,
    hue_range: f64,
    saturation_multiplier: f64,
    saturation_range: f64,
    value_multipler: f64,
    value_range: f64,
    array_len: usize,
) {
    set_panic_hook();
    let mut in_hsv_array = convert_rgb_to_hsv(in_rgb_array, array_len);
    let mut out_hsv_array = vec![0u16; array_len];
    adjust_image_hsv_wasm(&mut in_hsv_array, &mut out_hsv_array, hue_multiplier, hue_range, saturation_multiplier, saturation_range, value_multipler, value_range, array_len);
    let out_rgb_array_temp = convert_hsv_to_rgb(&out_hsv_array, out_hsv_array.len());
    for (i, val) in out_rgb_array_temp.iter().enumerate() {
        out_rgb_array[i] = *val;
    }
}
